#!/usr/bin/python3

import argparse
import os
import shutil
from os import path
from urllib.request import urlretrieve

import yaml
import shell
import utils
from utils import ask_yes_no


def install(name, version, url, folder, archive_path):
    destination = url.split('/')[-1]
    if not path.exists(destination):
        urlretrieve(url, destination)
    if not path.exists(path.join(folder, version)):
        shutil.unpack_archive(destination, folder)
        shutil.move(path.join(folder, archive_path), path.join(folder, version))
    os.remove(destination)
    if ask_yes_no(f"do you want to use {name} {version} by default?", default=True):
        symlink = path.join(folder, "default")
        if path.exists(symlink):
            os.remove(symlink)
        os.symlink(version, symlink)


def install_picat(version):
    url_version = version.replace('.', '')
    url = f"http://picat-lang.org/download/picat{url_version}_linux64.tar.gz"
    if not os.path.exists(f"Picat/{version}"):
        install("picat", version, url, "Picat", "Picat")
    # Todo install fzn_picat and fzn_picat_sat


def install_minizinc(version):
    url = f"https://github.com/MiniZinc/MiniZincIDE/releases/download/{version}/MiniZincIDE-{version}-bundle-linux-x86_64.tgz"
    if not os.path.exists(f"MiniZinc/{version}"):
        install("minizinc", version, url, "MiniZinc", f"MiniZincIDE-{version}-bundle-linux-x86_64")


def generate_env(dependencies):
    env_sh = f"""
    #!/usr/bin/bash
    # External dependencies
    export DEPS="$TAGADA_HOME/tagada-deps"
    ## Add custom minizinc to path
    export PATH="$DEPS/MiniZinc/{dependencies['minizinc']}/bin:$PATH"
    export MINIZINC="$DEPS/MiniZinc/{dependencies['minizinc']}/bin/minizinc"
    ## Add custom picat to pach
    export PATH="$DEPS/Picat/{dependencies['picat']}:$PATH"
    export PICAT="$DEPS/Picat/{dependencies['picat']}/picat:$PATH"
    export FZN_PICAT_SAT="$DEPS/Picat/{dependencies['picat']}/lib/fzn_picat_sat.pi"
    # export ESPRESSO_AB="espresso"
    """
    with open("env.sh", "w") as env_sh_file:
        env_sh_file.write(env_sh.strip())


if __name__ == '__main__':
    parser = argparse.ArgumentParser("install.py")
    parser.add_argument("-y", "--assumeyes", help="automatically answers yes to all questions", action="store_true")
    parser.add_argument("-v", "--verbose", help="enable verbosity", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        shell.VERBOSITY = True
    if args.assumeyes:
        utils.ASSUME_YES = True

    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
    dependencies = config["dependencies"]
    # Install MiniZinc
    install_minizinc(dependencies["minizinc"])
    # Install Picat
    install_picat(dependencies["picat"])
    # Generate env.sh file
    generate_env(dependencies)
